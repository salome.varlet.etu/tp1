"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; // function formatageP (prix){
//     if(prix%1==0) return (prix+".00").toString();
//     let pmod=prix%1;
//     if((pmod*10)%1==0) return (prix+"0").toString();
//     return prix;
// }

data.sort(function compare(a, b) {
  if (a.price_small < b.price_small) return -1;
  if (a.price_small > b.price_small) return 1;

  if (a.price_small == b.price_small) {
    if (a.price_large < b.price_large) return -1;
    if (a.price_large > b.price_large) return 1;
  }

  return 0;
});
var tom = data.filter(function (data) {
  return data.name.toLocaleLowerCase().split("i").length - 1 >= 2;
});
var html = ' ';

for (var i = 0; i < tom.length; i++) {
  html += "<article class=\"pizzaThumbnail\">\n\t<a href=\"\">\n\t\t<img src=\"".concat(tom[i].image, "\" />\n\t\t<section>\n\t\t\t<h4>").concat(tom[i].name, "</h4>\n\t\t\t<ul>\n\t\t\t\t<li>Prix petit format : ").concat(tom[i].price_small.toFixed(2), " \u20AC</li>\n\t\t\t\t<li>Prix grand format : ").concat(tom[i].price_large.toFixed(2), " \u20AC</li>\n\t\t\t</ul>\n\t\t</section>\n\t</a>\n</article>");
}

;
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map